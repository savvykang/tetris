package TetrisServer;

import java.util.ArrayList;

public class RoomList {
	ArrayList<Room> RoomList=null;
	int roomNumber = 1;

	public RoomList() {
		RoomList = new ArrayList<Room>();
	}

	public void roomAddRoomList(Room room) { // RoomLsit 목록에 Room 추가.
		RoomList.add(room);
		room.setRoomNumber(roomNumber++);
	}

	public void roomRemoveRoomList(Room room) { // RoomLsit 목록에 Room 삭제.
		RoomList.remove(room);
		room.setRoomNumber(roomNumber--);
	}

	public String getRoomList() { // 대기실에 존재하는 사용자를 추출하여 리턴.
		String list= "";
		for (Room room : RoomList) {
			list += (room.getRoomNumber() + ":" + room.getRoomTitle() + ":"
					+ room.getPeoplesStr() + ":");
		}
		return list;
	}
	
	public ArrayList<Room> getRoom() {
		return RoomList;
	}
	
	public Room getRoomByNumber(int number) { // 방 번호에 해당하는 방 정보 리턴
		for (Room room : RoomList)
			if (room.getRoomNumber() == number)
				return room;
		return null;
	}
}