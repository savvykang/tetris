package TetrisServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.StringTokenizer;

public class User implements Runnable {
	Socket socket;
	Server server;
	BufferedReader Rx;
	PrintWriter Tx;
	Protocol protocol;
	String RxStr = null;
	String Username;
	Thread thread;
	
	public String toString() {
		return Username;
	}

	public User(Socket socket, Server server) { // User 생성
		this.socket = socket;
		this.server = server;
		this.protocol = new Protocol();
		try {
			this.Rx = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			this.Tx = new PrintWriter(socket.getOutputStream(), true);
		} catch (IOException e) {
			System.out.println("<Server> 생성중  예외 발생 : " + e);
		}
		thread = new Thread(this);
	}

	public void run() { // 수신
		try {
			while ((RxStr = Rx.readLine()) != null) {
				server.monitoringTA.append("Client  송신 : " + RxStr + "\n");
				RxMessageParsing(RxStr);
			}
		} catch (IOException e) {
			server.userlist.userRemoveUserList(this);
		}
	}

	public void RxMessageParsing(String msg) {
		String[] data = msg.split(":");
		int action = Integer.parseInt(data[1]);
		Room room;
		switch (action) {
			case Protocol.ROOM_OUT_REQUEST : {
				String playerNumer = data[2];
				int roomNumber = Integer.valueOf(data[3]);
				room = server.roomlist.getRoomByNumber(roomNumber);
				server.userlist.userAddUserList(this);
				room.removeUserInRoom(this);
				if (room.getPeoplesInt() == 0)
					server.roomlist.roomRemoveRoomList(room);
				else {
					for (User user : room.users) {
						String str = protocol.ROOM_OUT_RESPOND(playerNumer);
						user.send(str);
					}
				}
				break;
			}
	
			case Protocol.ROOM_STATE_REQUEST: {
				int number = Integer.valueOf(data[3]);
				room = server.roomlist.getRoomByNumber(number);
				int Full = room.getFull();
				int gamming = room.getGamming();
				send(protocol.ROOM_STATE_RESPOND(data[2], number, Full, gamming));
				break;
			}
	
			case Protocol.ROOM_IN_REQUEST: {
				int number = Integer.valueOf(data[3]);
				room = server.roomlist.getRoomByNumber(number);
				String title = room.getRoomTitle();
				room.addUserInRoom(this);
				server.userlist.userRemoveUserList(this);
				send(protocol.ROOM_IN_RESPOND(data[2], number, title));
				break;
			}
	
	
			case Protocol.GAME_KEYEVENT_REQUEST: {
				int number = Integer.valueOf(data[3]);
				room = server.roomlist.getRoomByNumber(number);
				int key = Integer.parseInt(data[4]);
				for (User user : room.users) {
					String str = protocol.GAME_KEYEVENT_RESPOND(data[2], number,
							key);
					user.send(str);
				}
				break;
			}
	
			case Protocol.GAME_INIT_BLOCK_REQUEST: {
				int number = Integer.valueOf(data[3]);
				room = server.roomlist.getRoomByNumber(number);
				Random r = new Random();
				int blockType = r.nextInt(7);
				
				for (User user : room.users) {
					String str = protocol.GAME_INIT_BLOCK_RESPOND(data[2], number,blockType);
					user.send(str);
				}
				break;
			}
	
			case Protocol.GAME_START_REQUEST : {
				String userName = data[2];
				int roomNumber = Integer.valueOf(data[3]);
				room = server.roomlist.getRoomByNumber(roomNumber);
				room.setGamming(1);
				ArrayList<Integer> blocksType = new ArrayList<Integer>(); // 난수를
				Random random = new Random();
				for (User user : room.users) // 방에 있는 사용자 수만큼 난수 발랭 루틴
					blocksType.add(random.nextInt(7)); // 0 ~ 6 까지 난수 생성
				String roomMemberBlocks = iteratorToString(blocksType.iterator(), ":");	
				String roomMemberNames = iteratorToString(room.users.iterator(), ":");
				String str = protocol.GAME_START_RESPOND(userName, roomNumber,
						roomMemberNames, roomMemberBlocks);
				for (User user : room.users)
					user.send(str);
				break;
			}
	
			case Protocol.CLIENT_LOGIN_REQUEST: {
				this.Username = data[2];
				send(protocol.CLIENT_LOGIN_RESPOND(Username));
				break;
			}
	
			case Protocol.USER_LIST_REQUEST: {
				send(protocol.USER_LIST_RESPOND(server.userlist.getUserList()));
				break;
			}
	
			case Protocol.ROOM_LIST_REQUEST: {
				send(protocol.ROOM_LIST_RESPOND(server.roomlist.getRoomList()));
				break;
			}
	
			case Protocol.ROOM_CREATE_REQUEST: {
				String userName = data[2];
				String roomTitle = data[3];
				room = new Room(roomTitle);
				server.roomlist.roomAddRoomList(room);
				int roomNumber = room.getRoomNumber();
				room.addUserInRoom(this); // 방 참가
				server.userlist.userRemoveUserList(this); // 유저 리스트 삭제
				send(protocol.ROOM_CREATE_RESPOND(userName, roomNumber, roomTitle));
				break;
			}
			case Protocol.ROOM_CHAT_REQUEST	: {
				String playerName = data[2];
				int roomNumber = Integer.valueOf(data[3]);
				String message = data[4];
				room = server.roomlist.getRoomByNumber(roomNumber);
				String str = protocol.ROOM_CHAT_RESPOND(playerName, message);
				
				for (User user : room.users)
					user.send(str);
				break;
			}
		}
	}

	public void send(String str) { // 송신
		Tx.println(str);
		server.monitoringTA.append("\t└─────────────Server 송신 : " + str + "\n");
	}
	public static String iteratorToString(Iterator iterator, String divider) { // Iterator - boolean hasNext, Object Next
		StringBuilder sb = new StringBuilder(); // StringBuilder(메소드 비동기) - 데이터 처리 속도 : 고속 , 데이터 안전성 : 불안정
		//StringBuffer sb = new StringBuffer(); // StringBuffer(메소드 동기) - 데이터 처리 속도 : 저속 , 데이터 안전성 : 안정
		while(iterator.hasNext()) { // 다음 요소가 있는 경우에 true
			sb.append(iterator.next().toString()); // 다음의 요소를 돌려줌
			if (iterator.hasNext()) { 
				sb.append(divider); // 구분자를 붙임
			}
		}
		return sb.toString(); // toString()하는 이유 : append 하여도 stack 처럼 쌓여서 String 형으로 변환해줘야 함
	}
}