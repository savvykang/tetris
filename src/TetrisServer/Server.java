package TetrisServer;

import java.awt.TextArea;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JFrame;

public class Server extends JFrame {
	TextArea monitoringTA;
	Socket socket;
	ServerSocket ss;
	UserList userlist;
	RoomList roomlist;

	public Server() {
		super("ServerScreen");
		try {
			this.ss = new ServerSocket(5000);
			this.userlist = new UserList();
			this.roomlist = new RoomList();
			setGUI();
			append("서버가 시작됩니다.");
			while (true) {
				this.socket = ss.accept();
				User user = new User(socket, this);
				userlist.userAddUserList(user);
				user.thread.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 500);
		monitoringTA = new TextArea("", 5, 5, TextArea.SCROLLBARS_VERTICAL_ONLY);
		monitoringTA.setEditable(false);
		add(monitoringTA, "Center");
		setVisible(true);
	}

	public void append(String str) {
		monitoringTA.append(str + "\n");
	}

	public static void main(String args[]) {
		new Server();
	}
}
