package TetrisServer;

import java.util.ArrayList;

public class UserList {
	ArrayList<User> UserList=null;

	public UserList() {
		UserList = new ArrayList<User>();
	}

	public void userAddUserList(User user) { // 대기실 목록에서 사용자 추가.
		UserList.add(user);
	}

	public void userRemoveUserList(User user) { // 대기실 목록에서 사용자 삭제.
		UserList.remove(user);
	}

	public String getUserList() { // 대기실에 존재하는 사용자를 추출하여 리턴.
		String list = "";
		for (User user : UserList)
			list += user.Username + ":";
		return list;
	}
}
