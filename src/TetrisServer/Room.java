package TetrisServer;

import java.util.ArrayList;

public class Room {
	ArrayList<User> users=null;
	private String roomTitle = "";
	private int roomNumber = 0;
	private int peoples = 0;
	private int full = 0;
	private int gamming = 0;

	public Room(String RoomTitle) {
		this.roomTitle = RoomTitle;
		this.users = new ArrayList<User>(); // 방에 사람을 저장해두는 백터 클래스
	}

	public void addUserInRoom(User user) { // Room In
		users.add(user);
		peoples++;
		if (peoples == 5)
			full = 1;
	}

	public void removeUserInRoom(User user) { // Room Out
		users.remove(user);
		peoples--;
		if (peoples < 5)
			full = 0;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public void setGamming(int gamming) {
		this.gamming = gamming;
	}

	public String getPeoplesStr() {
		return "(" + peoples + "/5)";
	}

	public String getRoomTitle() {
		return this.roomTitle;
	}

	public int getPeoplesInt() {
		return this.peoples;
	}

	public int getRoomNumber() {
		return this.roomNumber;
	}

	public int getGamming() {
		return this.gamming;
	}

	public int getFull() {
		return full;
	}
}
