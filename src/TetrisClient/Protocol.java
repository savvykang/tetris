package TetrisClient;

public class Protocol {
	static final int STX = 0xF1; 
	static final int ETX = 0xF2;
	static final int CLIENT_LOGIN_REQUEST	 = 	1; 
	static final int CLIENT_LOGIN_RESPOND	 = 	2; 
	static final int ROOM_STATE_REQUEST 	 = 	3;
	static final int ROOM_STATE_RESPOND 	 =  4;
	static final int USER_LIST_REQUEST 		 =  5; 
	static final int USER_LIST_RESPOND 		 =  6;
	static final int ROOM_LIST_REQUEST		 =  7; 
	static final int ROOM_LIST_RESPOND 		 =  8;
	static final int ROOM_CREATE_REQUEST 	 =  9; 
	static final int ROOM_CREATE_RESPOND 	 = 10;
	static final int ROOM_IN_REQUEST 		 = 11; 
	static final int ROOM_IN_RESPOND 		 = 12;
	static final int ROOM_OUT_REQUEST 		 = 13;
	static final int ROOM_OUT_RESPOND  		 = 14;
	static final int ROOM_CHAT_REQUEST 		 = 15;
	static final int ROOM_CHAT_RESPOND 		 = 16;
	static final int GAME_START_REQUEST 	 = 17;
	static final int GAME_START_RESPOND 	 = 18;
	static final int GAME_KEYEVENT_REQUEST 	 = 19;
	static final int GAME_KEYEVENT_RESPOND 	 = 20;
	static final int GAME_INIT_BLOCK_REQUEST = 21;
	static final int GAME_INIT_BLOCK_RESPOND = 22;
	
	public String CLIENT_LOGIN_REQUEST(String userName) {
		return STX + ":" + CLIENT_LOGIN_REQUEST + ":" + userName + ":" + ETX;
	}
	
	public String CLIENT_LOGIN_RESPOND(String userName) {
		return STX + ":" + CLIENT_LOGIN_RESPOND + ":" + userName + ":" + ETX;
	}
	///////////////////////////////////////////////////////////////////////////
	public String ROOM_STATE_REQUEST(String userName, int roomNumber) {
		return STX + ":" + ROOM_STATE_REQUEST + ":" + userName + ":" + roomNumber
				+ ":" + ETX;
	}
	
	public String ROOM_STATE_RESPOND(String userName, int roomNumber, int full, int gammming) {
		return STX + ":" + ROOM_STATE_RESPOND + ":" + userName + ":" + roomNumber + ":" + full + ":" + gammming + ":" + ETX;
	}
	///////////////////////////////////////////////////////////////////////////
	public String USER_LIST_REQUEST(String userName) {
		return STX + ":" + USER_LIST_REQUEST + ":" + userName + ":" + ETX;
	}
	
	public String USER_LIST_RESPOND(String userList) {
		return STX + ":" + USER_LIST_RESPOND + ":" + userList + ":" + ETX;
	}
	///////////////////////////////////////////////////////////////////////////
	public String ROOM_LIST_REQUEST(String userName) {
		return STX + ":" + ROOM_LIST_REQUEST + ":" + userName + ":" + ETX;
	}
	
	public String ROOM_LIST_RESPOND(String roomList) {
		return STX + ":" + ROOM_LIST_RESPOND + ":" + roomList + ":" + ETX;
	}
	///////////////////////////////////////////////////////////////////////////	
	public String ROOM_CREATE_REQUEST(String userName, String roomTitle) {
		return STX + ":" + ROOM_CREATE_REQUEST + ":" + userName + ":"
				+ roomTitle + ":" + ETX;
	}
	
	public String ROOM_CREATE_RESPOND(String userName, int roomNumber, String roomTitle) {
		return STX + ":" + ROOM_CREATE_RESPOND + ":" + userName + ":" + roomNumber + ":" + roomTitle + ":" + ETX;
	}
	///////////////////////////////////////////////////////////////////////////
	public String ROOM_IN_REQUEST(String userName, int roomNumber) {
		return STX + ":" + ROOM_IN_REQUEST + ":" + userName + ":" + roomNumber
				+ ":" + ETX;
	}
	
	public String ROOM_IN_RESPOND(String userName, int roomNumber, String roomTitle) {
		return STX + ":" + ROOM_IN_RESPOND + ":" + userName + ":" + roomNumber + ":" + roomTitle
				+ ":" + ETX;
	}
	///////////////////////////////////////////////////////////////////////////
	public String ROOM_OUT_REQUEST(String userName, int roomNumber) {
		return STX + ":" + ROOM_OUT_REQUEST + ":" + userName + ":" + roomNumber
				+ ":" + ETX;
	}
	
	public String ROOM_OUT_RESPOND(String userName) {
		return STX + ":" + ROOM_OUT_RESPOND + ":" + userName + ":" + ETX;
	}
	///////////////////////////////////////////////////////////////////////////
	public String ROOM_CHAT_REQUEST(String userName, int roomNumber, String message) {
		return STX + ":" + ROOM_CHAT_REQUEST + ":" + userName + ":" + roomNumber + 
				":" + message + ":" + ETX;
	}
	
	public String ROOM_CHAT_RESPOND(String userName, String message) {
		return STX + ":" + ROOM_CHAT_RESPOND + ":" + userName + ":" + message + ":" + ETX;
	}
	///////////////////////////////////////////////////////////////////////////
	public String GAME_START_REQUEST(String userName, int roomNumber) {
		return STX + ":" + GAME_START_REQUEST + ":" + userName + ":" + roomNumber +
				":" + ETX;
	}
	
	public String GAME_START_RESPOND (String userName, int roomNumber, String roomMemberNames, String roomMemberBlocks) {
		return STX + ":" + GAME_START_RESPOND + ":" + userName + ":" +
				roomNumber + ":" + roomMemberNames + ":" + roomMemberBlocks + ":" + ETX;
	}
	///////////////////////////////////////////////////////////////////////////
	public String GAME_KEYEVENT_REQUEST(String userName, int roomNumber, int key) {
		return STX + ":" + GAME_KEYEVENT_REQUEST + ":" + userName + ":" + roomNumber
				+ ":" + key + ":" + ETX; 
	}
	
	public String GAME_KEYEVENT_RESPOND(String userName, int roomNumber, int key) {
		return STX + ":" + GAME_KEYEVENT_RESPOND + ":" + userName + ":" + roomNumber + ":" + key + ":" + ETX;
	}
	///////////////////////////////////////////////////////////////////////////
	public String GAME_INIT_BLOCK_REQUEST(String userName, int roomNumber) {
		return STX + ":" + GAME_INIT_BLOCK_REQUEST+ ":" + userName + ":" + roomNumber
				+ ":" + ETX;
	}
	
	public String GAME_INIT_BLOCK_RESPOND(String userName, int roomNumber, int blockType) {
		return STX + ":" + GAME_INIT_BLOCK_RESPOND + ":" + userName + ":" + roomNumber
				+ ":" + blockType + ":" + ETX;
	}	
}