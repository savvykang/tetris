package TetrisClient;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

import TetrisClient.GameScreen.IntBlocReuqsetHandler;

class BoardGraphics extends JPanel {
	int pixelSize;                                                 			    // 하나의 pixel이 가지는 Size
	Board board;                                                   			    // 게임 진행 상황을 나
	Graphics bufferg;                                              			    // 그림을 그리기 위한 도구
	Image buffer;                                                  			    // 더블버퍼링을 위한 버퍼 이미지
	long time = 0;                                                 			    // 게임화면을 화면에 그리는 시간을 조정하는  변수
	IntBlocReuqsetHandler IntBlocReuqsetHandler;                                // 새로운 블럭을 서버로 부터 받기위함 
	
	public BoardGraphics(int pixelSize) {                 
		board = new Board();                          
		this.pixelSize = pixelSize;
	}

	public void keyProcess(int key) {                              			    // Key Rx
		board.key = key;                                            			// 보드에 현재 발생한 이벤트키를 저장
		board.removeBlock(board.block);                             			// 보드에 현재 블록을 지움
		board.block.move(key);                                      			// 블럭을 이벤트에 따라 움직임
		boolean bool = board.preProcess(board.block);               			// 이벤트에 따라 처리된 블럭이 보드에  유효한지 검증하고, 전처리
		board.addBlock(board.block);                                			// 전처리된 블럭을 다시 보드에 더함
		
		if ( bool ) {                                               			// 블럭이 밑바닥에서 한칸 더 내려가려는 경우, 이동이 VK_DOWN이면 게임이 끝나지 않는 경우 - true
			board.checkLine();                                      			// 생성전에 제거된 라인이 있는지 확인하고 있다면 제거
			
			if ( pixelSize == 20 ) {                                   			// 오직 자신의 화면에만 발생하도록 하기 위함
				IntBlocReuqsetHandler.actionPerformed(null);
				System.out.println("1");
			}
		}
		repaint();                                                  			// 프레임에 이미지를 그림
	}

	public void drawBuffer() {                                      			// Key Rx GUI
		for ( int i = 0, rows = board.row; i < rows; i++ ) {         			// 보드에 기록된 배열의 색상을	 퍼에 그린다.
			for ( int j = 0, cols = board.col; j < cols; j++ ) {
				int x = pixelSize * j;
				int y = pixelSize * i;
				bufferg.setColor(board.boardColor[i][j]);
				bufferg.fillRect(x, y, pixelSize, pixelSize);
			}
		}
	}

	public void paint(Graphics g) {                                 			// 버퍼의 그림을 실제 Frame에 그림
		if (buffer == null) {
			buffer = createImage(getWidth(), getHeight());          			// 오프 스크린 draw 이미지를  생성
			bufferg = buffer.getGraphics();                        			    // 오프 스크린 이미지에 draw하기 위한 크래픽스 문잭을 작성하기위함
		}
		
		long currentTimeMillis = System.currentTimeMillis();       
		if ( buffer != null && currentTimeMillis-time>1000/120 ) {  			// 일정 시간 간격으로 게임화면을 그림
			drawBuffer();
			g.drawImage(buffer, 0, 0, this);
			time = currentTimeMillis;
		}
	}
}