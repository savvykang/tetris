package TetrisClient;

import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class JButtonTableCellRenderer extends JButton implements
		TableCellRenderer {
	public JButtonTableCellRenderer() {
		setRolloverEnabled(true);
	}

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		String buttonText = (String) value;
		setText(buttonText);
		getModel().setRollover(hasFocus);
		return this;
	}
}