package TetrisClient;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class Board {
	public static int row = 20;                                      			// 보드의 세로길이
	public static int col = 10;                 			         			// 보드의 가로길이
	public Color defualtColor = Color.black;     			         			// 보드 배경색상 지정
	public Color boardColor[][];                 			         			// 보드의 색상배열 세로*가로의 크기
	public Block block;                          			        			 // 보드에 출력한 블록
	int key;                                     			         			// 현재 보드에 발생한 이벤트
	ActionListener gameOverHandler;
	
	public Board() {                             			         			// 보드 생성 & 보드 배경색 지정
		boardColor = new Color[row][col];        			         			// 가로 , 세로 크기 만큼 배열을 생성
		
		for(int i = 0; i < row; i++)             			   	     			// 배열크기만큼 배경색으로 보드를 초기화
			for(int j = 0; j < col; j++)
				boardColor[i][j] = defualtColor;                     			// defualtColor인 검은색으로 셋팅	
	}
	
	public void initBlock(int blockType) {       
		block = new Block(blockType);	         			         			// 블럭 생성
		addBlock(block);                         			         			// 생성된 블럭을 보드에 추가
	} 
	
	public void addBlock(Block block){                               			// 보드에 생성된 블럭 추가
		Point ap = block.absolute;                                   			// 기준이 되는 절대 좌표
		
		for( int i = 0; i < 4; i++ ) {
			Point rp = block.relative[i];                            			// 블럭의 모양을 나타내는 상대 좌표
			boardColor[ap.y+rp.y][ap.x+rp.x] = block.color;          			// 블럭이 위치하는곳에 색상을 블럭 고유 색상으로 변경
		}
	}
	
	public void removeBlock(Block block){                            			// 보드에서 현재 블럭을 삭제
		Point ap = block.absolute;                                   			// 기준이 되는 절대 좌표  
		
		for( int i = 0; i < 4; i++ ) {
			Point rp = block.relative[i];                            			// 블럭의 모양을 나타내는 상대 좌표
			boardColor[ap.y+rp.y][ap.x+rp.x] = defualtColor;         			// 블럭이 위치하는곳에 색상을 배경색으로 변경
		}
	}
	
	public void checkLine() {                                       			 // 한 라인이 배경색이 아닌 색으로 체워저 있다면, 그 라인을 삭제
		for( int i = 0; i < row; i++ ) {
			int count = 0;                                          			 // 한 row에 색상이 있는 col를 카운터하기 위함
			
			for(int j = 0; j < col; j++)                          
				if(boardColor[i][j] != defualtColor)                 			// boardColor가 기본 색상이 아니라면 count 증가
					count++;
			if(count == col)                                         			// count가 col수 만큼 증가 했다는건 해당 라인 모두 배경색이 아니므로 라인 삭제
				deleteLine(i);
		}
	}	
	
	private void deleteLine(int row) {                               			// 해당 라인은 위의 라인으로 체우고 맨위 라인은 배경색으로 채움
		for( int i = row; i > 0; i-- )                            
			for( int j = 0; j < col; j++ )
				boardColor[i][j] = boardColor[i-1][j];              			 // board에 설정된 색상들을 한캄씩 밑으로 이동       
		for(int i = 0; i < col; i++)                              
			boardColor[0][i] = defualtColor;	                     			// 가장 위에 존재 하는 라인을 기본 색상으로 설정
	}	

	public boolean preProcess(Block block) {                        			 // 이벤트 발생시 블럭이 보드에 유효한지 체크
		Point ap = block.absolute;
		
		for( int i = 0; i < 4; i++ ) {                               			// 블럭의 각 영역  체크
			Point rp = block.relative[i]; 
			
			if( ap.y + rp.y >= row ) {                               			// 블럭이 밑바닥에서 한칸 더 내려가려는 경우
				block.moveBack(key);                                 			// 이전에 위치했던 곳으로 돌림
				return true;                                         			// 현재 블럭은 종료하고 새블럭 생성 (true 리턴)
			}
			
			if( ap.x+rp.x<0 || ap.x+rp.x>=col ) {                    			// ap.x+rp.x<0 : 좌측 벽, ap.x+rp.x>=col : 우측벽 
				block.moveBack(key);                                 			// 더이상 left,right이 될수 없는경우는 전 블록으로 복귀
				return false;                                        			// 해당 블럭으로 계속 진행
			}
			
			if( boardColor[ap.y+rp.y][ap.x+rp.x] != defualtColor ) { 			// 이동시 다른 블럭이 존재 할경우
				if( key == KeyEvent.VK_DOWN ) {                     			// 이동이 VK_DOWN일 경우
					if ( ap.y+rp.y == 1 ) {                          			// ap.y+rp.y ==1 : 위쪽 벽 (게임 종료) - 스레드 종료 & KeyEnvent 발생(금지)
						if (gameOverHandler != null) {
							gameOverHandler.actionPerformed(null);
						}
					} else {                                         			// 이동이 VK_DOWN이면 게임이 끝나지 않는 경우
						block.moveBack(key);                         			// 이전에 위치했던 곳으로 돌림     
						return true;                                 			// 현재 블럭은 종료, 새블럭 생성(true 리턴)
					}
				} else {                                             			// 이동이 다른 이벤트 일경우
					block.moveBack(key);                             			// 이동하려는 곳이 이미 다른 블럭이 있는경우 전 블록으로 복귀
					return false;                                    			// 해당 블럭으로 계속 진행
				}
			}
		}                                                            
		return false;                                                			// 전처리 할대상이 없다면, 블록은 계속 진행
	}
}
