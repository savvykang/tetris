package TetrisClient;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

public class JButtonTableCellEditor extends AbstractCellEditor implements
		TableCellEditor {
	private JButton button;

	public JButtonTableCellEditor() {
		button = new InternalButton();
	}

	public void addActionListener(ActionListener l) {
		button.addActionListener(l);
	}

	public void removeActionListener(ActionListener l) {
		button.removeActionListener(l);
	}

	public Component getTableCellEditorComponent(JTable table, Object value,
		boolean isSelected, int row, int column) {
		String buttonText = (String) value;
		button.setText(buttonText);
		return button;
	}

	public Object getCellEditorValue() {
		return button.getText();
	}

	private class InternalButton extends JButton {
		protected void fireActionPerformed(ActionEvent event) {
			super.fireActionPerformed(event);
			fireEditingStopped();
		}
	}
}