package TetrisClient;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.KeyEvent;

public class Block implements Cloneable {
	public static final Color[] colors={Color.blue, Color.CYAN, Color.GRAY, Color.GREEN, Color.ORANGE, Color.PINK, Color.RED}; // 블럭의 생상
	public static final int L_PIECE   = 0;  	  								// 각 블럭의 상수화
	public static final int J_PIECE   = 1;
	public static final int I_PIECE   = 2;
	public static final int Z_PIECE   = 3;
	public static final int S_PIECE   = 4;
	public static final int O_PIECE   = 5;
	public static final int T_PIECE   = 6; 
	private int blcokType;                   	  								// 블럭의 모양(0~6)
	Point absolute;                          	  								// 절대 위치 (보드에서 기준이되는 좌표)
	Point[] relative;                             								// 상대위치 (절대위치에 더해서 보드에 위치할 좌표)
	Color color;                                  								// 블럭의 생상
	
	public Block(int blcokType) {                 								// 블럭이 가지는 초기값 설정
		this.blcokType = blcokType;               								// 블럭의 모양 초기화
		this.absolute = new Point(Board.col/2,0); 								// 절대 잠표로서 기준이되는 좌표
		this.relative = new Point[4];             								// 상대 좌표로서 절대 좌표에 따라 블럭을 나타냄
		this.color = colors[blcokType];           								// 블럭의 모양에 맞게 색상이 설정
		initRelative();
	}
	
	private void initRelative() {                 								// 블럭 타입(0~6)에 따른 상대 위치
		switch (blcokType) {
			case I_PIECE : {                      
				relative[0] = new Point( 0, 0);
				relative[1] = new Point(-1, 0);
				relative[2] = new Point( 1, 0);
				relative[3] = new Point( 2, 0);
				break; 
			}

			case L_PIECE : { 
				relative[0] = new Point( 0, 0);
				relative[1] = new Point(-1, 0);
				relative[2] = new Point(-1, 1);
				relative[3] = new Point( 1, 0);
				break; 
			}

			case J_PIECE : { 
				relative[0] = new Point( 0, 0);
				relative[1] = new Point(-1, 0);
				relative[2] = new Point( 1, 0);
				relative[3] = new Point( 1, 1);
				break; 
			}
	
			case Z_PIECE : { 
				relative[0] = new Point( 0, 0);
				relative[1] = new Point(-1, 0);
				relative[2] = new Point( 0, 1);
				relative[3] = new Point( 1, 1);
				break; 
			}
	
			case S_PIECE : { 
				relative[0] = new Point( 0, 0);
				relative[1] = new Point( 1, 0);
				relative[2] = new Point( 0, 1);
				relative[3] = new Point(-1, 1);
				break; 
			}
	
			case O_PIECE : { 
				relative[0] = new Point( 0, 0);
				relative[1] = new Point( 0, 1);
				relative[2] = new Point(-1, 0);
				relative[3] = new Point(-1, 1);
				break; 
			}
	
			case T_PIECE : { 
				relative[0] = new Point( 0, 0);
				relative[1] = new Point(-1, 0);
				relative[2] = new Point( 1, 0);
				relative[3] = new Point( 0, 1);
				break; 
			}
		}
	}
	
	private void rotate() {                       								// VK_UP에 따른 회전 
		for ( int count=0 ; count<4 ; count++ ) {
			int temp = relative[count].x;
			relative[count].x = -relative[count].y;
			relative[count].y = temp;
		}
	}
	
	private void rotateBack() {                   								// VK_UP버튼 움직임을 되돌림(rotate 복귀)
		for ( int count = 0; count < 4; count++ ) {
			int temp = relative[count].x;
			relative[count].x = relative[count].y;
			relative[count].y = -temp;
		}	
	}
	
	public void move(int keyCode) {               								// 이벤트에 따라 블럭의 절대 좌표 이동
		switch(keyCode){
			case KeyEvent.VK_LEFT  : absolute.x--; 
									 break;
			case KeyEvent.VK_RIGHT : absolute.x++;
									 break;
			case KeyEvent.VK_DOWN  : absolute.y++;
									 break;
			case KeyEvent.VK_UP    : rotate();
									 break;
			}
	}
	
	public void moveBack(int keyCode) {           								// 이벤트에 따라 블럭의 절대 좌표 이동(이동 제한이 되었을떄)
		switch(keyCode){
			case KeyEvent.VK_LEFT  : absolute.x++;
				                     break;
			case KeyEvent.VK_RIGHT : absolute.x--;
				                     break;
			case KeyEvent.VK_DOWN  : absolute.y--;
				                     break;
			case KeyEvent.VK_UP    : rotateBack();
				                     break;
		}
	}
}
