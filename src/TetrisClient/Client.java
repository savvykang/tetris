package TetrisClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Client implements Runnable {
	Socket socket;                                                              // 통신 소켓
	BufferedReader Rx;                                                          // 수신 버퍼
	PrintWriter Tx;                                                             // 송신 버퍼
	String RxStr;                                                               // 수신 내용
	String TxStr;                                                               // 송신 내용
	String userName;                                                            // 사용자 이름
	Protocol protocol;                                                          // 규약
	ReadyScreen RS;                                                             // 대기 화면
	GameScreen GS;                                                              // 게임 화면
	
	public Client(String username, String serverIP) {                           // Client 생성 & 서버와 연결
		try {
				this.socket = new Socket(serverIP, 5000);                       // 통신 소켓
				this.Rx = new BufferedReader(new InputStreamReader(             // 수신 통로
						socket.getInputStream())); 
				this.Tx = new PrintWriter(socket.getOutputStream(), true);      // 송신 통로
				this.protocol = new Protocol();                                 // 규약
				this.userName = username;                                       // 사용자 이름
				send(protocol.CLIENT_LOGIN_REQUEST(userName));                  // ReadyScreen을 띄우기 위한 전송 프로토콜
				new Thread(this).start();                                       // 수신 시작
			} catch (IOException e) {                                           // 서버가 구동되지 않을때                                       
				JOptionPane.showMessageDialog(null, "서버와 연결을 실패하였습니다.", "연결 실패", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void run() {                                                         // 수신
		try {
			while ( (RxStr = Rx.readLine()) != null )
				RxMessageParsing(RxStr);                                        // 수신한 패킷을 메시지 파싱하여 분리 작업
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "서버와 연결이 끊어졌습니다.", "연결 끊김", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
	}
	
	public void send(String str) { // 송신
		Tx.println(str);
	}

	public void RxMessageParsing(String msg) {
		StringTokenizer st = new StringTokenizer(msg, ":");
		int cTokens = st.countTokens();
		String[] data = new String[cTokens];
		for (int i = 0; st.hasMoreTokens(); i++)
			data[i] = st.nextToken();
		int messageTpye = Integer.valueOf(data[1]);
		
		switch (messageTpye) {
			case Protocol.CLIENT_LOGIN_RESPOND : {
				this.RS = new ReadyScreen(this, protocol);
				send(protocol.ROOM_LIST_REQUEST(userName));
				send(protocol.USER_LIST_REQUEST(userName));
				break;
			}
			
			case Protocol.ROOM_CREATE_RESPOND : {
				String playerName = data[2];
				int roomNumber = Integer.parseInt(data[3]);
				this.GS = new GameScreen(this, roomNumber, data[4]);
				RS.setState(JFrame.ICONIFIED);
				break;
			}
			
			case Protocol.ROOM_IN_RESPOND : {
				String playerName = data[2];
				int roomNumber = Integer.parseInt(data[3]);
				String roomTitle = data[4];
				this.GS = new GameScreen(this, roomNumber, roomTitle);
				RS.setState(JFrame.ICONIFIED);
				break;
			}
			
			case Protocol.GAME_KEYEVENT_RESPOND : {
				String playerName = data[2];
				int roomNumber = Integer.parseInt(data[3]);
				int key = Integer.parseInt(data[4]);
				BoardGraphics view = GS.boardForUser.get(playerName);
				view.keyProcess(key);
				break;
			} 
			
			case Protocol.GAME_INIT_BLOCK_RESPOND : {
				String playerName = data[2];
				int roomNumber = Integer.parseInt(data[3]);
				int blockType = Integer.valueOf(data[4]);
				BoardGraphics view = GS.boardForUser.get(playerName); 
				
				if ( view != null ) {
					view.board.initBlock(blockType);
				}
				break;
			}
				
			case Protocol.GAME_START_RESPOND : {
				ArrayList<String> playerNames = new ArrayList<String>();
				ArrayList<Integer> blockTypes = new ArrayList<Integer>();
				GS.requestFocusInWindow();
				GS.startButton.setEnabled(false);
				int mid = (4 + cTokens - 2) / 2;
				int index = 0;
				
				for ( int i = 4; i <= mid; i++ ) 
					playerNames.add(data[i]);
				
				for ( int i = mid + 1; i < cTokens - 1; i++ )
					blockTypes.add(Integer.parseInt(data[i]));
				
				for ( int i = 0, people = playerNames.size(); i < people; i++ ) {
					String playerName = playerNames.get(i);
					int blockType = blockTypes.get(i);
					
					if (userName.equals(playerName)) {
						BoardGraphics view = GS.boardHolder[4];					
						view.board.initBlock(blockType);
					} else {
						BoardGraphics view = GS.boardHolder[index];					
						GS.boardForUser.put(playerName, view);
						view.board.initBlock(blockType);
						index++;
					}
				}
				GS.thread.start();
				break;
			}
			
			case Protocol.USER_LIST_RESPOND : {
				RS.userListTA.setText("");
				for ( int index = 2; index < (cTokens - 1); index++ ) {
					RS.userListTA.append(data[index] + "\n");
				}
				break;
			}
			
			case Protocol.ROOM_LIST_RESPOND : {
				if ( cTokens > 3 ) {
					int index = 2;
					RS.removeAllRow();
					
					for ( int i = 0; i < (cTokens / 3) - 1; i++ ) {
						RS.addRow(data[index], data[index + 1], data[index + 2]);
						index += 3;
					}
				} else {
					JOptionPane.showMessageDialog(null, "현재 개설된 방이 없습니다.", "", JOptionPane.ERROR_MESSAGE);
				}
				break;
			}
			
			case Protocol.ROOM_STATE_RESPOND : {
				String playerName = data[2];
				int roomNumber = Integer.parseInt(data[3]);
				int full = Integer.parseInt(data[4]);
				int gamming =  Integer.parseInt(data[5]);
				
				if ( full == 1) {
					JOptionPane.showMessageDialog(null, "인원이 가득차 입장 할 수 없습니다.", "", JOptionPane.ERROR_MESSAGE);
				} else if ( gamming == 1 ) {
					JOptionPane.showMessageDialog(null, "게임중이라 입장 할 수 없습니다.", "", JOptionPane.ERROR_MESSAGE);
				} else {
					send(protocol.ROOM_IN_REQUEST(playerName, roomNumber));
				}
				break;
			}
			
			case Protocol.ROOM_CHAT_RESPOND : {
				String playerName = data[2];
				String message = data[3];
				GS.myAppend(playerName, message);
			}
		}
	}	
	public static void main(String[] args) {
		String userName;
		String serverIP;
		
		while (true) {                                                          // 서버 IP
			serverIP = JOptionPane.showInputDialog(null, "서버 IP를 입력하십시오. (빈칸은 localhost)");
			
			if(serverIP == null)                                                // 닫기(아이콘) & 취소 누를시 
				System.exit(0);
			else if ( serverIP.length() < 1 ) {                                 // 공백으로 입력시
				serverIP = "localhost";
				break;
			} else if ( serverIP.length() > 1 && serverIP.length() < 11 ) {
				JOptionPane.showMessageDialog(null, "올바른 IP 주소를 입력하십시오.","서버 IP 오류", JOptionPane.ERROR_MESSAGE);
			} else                                                              // serverIP를 입력 했을 경우
				break;
		}
		
		while (true) {                                                          // 사용자 이름 입력
			userName = JOptionPane.showInputDialog(null, "사용자의 이름을 입력하십시오.");
			
			if ( userName == null ) {                                           // 닫기(아이콘) & 취소 누를시 
				System.exit(0);
			} else if ( userName.length() < 1 ) {                               // 공백으로 입력시
				JOptionPane.showMessageDialog(null, "사용자 이름을 입력 하지 않았습니다.", "사용자 이름 공백", JOptionPane.ERROR_MESSAGE);
			} else                                                              // 이름을 입력 했을 경우
				break;
		}
		new Client(userName, serverIP);	
	}
}