package TetrisClient;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class GameScreen extends javax.swing.JFrame  implements KeyListener {
	Map<String, BoardGraphics> boardForUser = new TreeMap<String, BoardGraphics>(); // 사용자 이름에 맞는 게임화면을 찾기 위함
	BoardGraphics[] boardHolder = new BoardGraphics[5];                             // 사용자들의 게임화면 (단, 5번째 게임화면은 자신의 게임화면)
	Protocol protocol;                                                              // 규악
	Thread thread;                                                                  // 게임 진행 스레드 : 주기적인 VK_DOWN 발생
	int roomNumber;                                                                 // 방 번호
	String roomTitle;                                                               // 방 제목
	Client client;                                                                  // 사용자
	JButton startButton;                                                            // 게임 시작 버튼	
	JButton chatButton;
	JLabel jLabel1;
    JLabel jLabel2;
    JLabel jLabel3;
    JLabel jLabel4;
    JScrollPane jScrollPane;
    JTextArea chatTA;
    JTextField chatTF;
    
    
    public GameScreen(final Client client, final int roomNumber,
			String roomTitle) {
    	super("♣ GameScreen ♣      " + "Number : " + roomNumber + "  Title : " + roomTitle + "  Nmae : " + client.userName);
		this.roomNumber = roomNumber;
		this.protocol = new Protocol();
		this.roomTitle = roomTitle;
		this.client = client;
		this.setLocationRelativeTo(null);
		initBoardGraphics();												    // GameScreen에 필요 5명의 게임화면을 생성하기 위함
		initComponents();
		this.startButton.addActionListener(new StartButtonHandler());			// 게임시작시 게임에 필요한 사용자들의 이름과 초기 블럭을 부여 받음
		this.addWindowListener(new MyWindowHandler()); 							// 방을 나갈시 ReadyScreen을 다시 불러오기 위함
		this.addKeyListener(this); 												// 게임 진행시 발생되는 키보드 이벤트를 위함
		this.setLocationRelativeTo(null);										// 게임 화면을 화면의 중앙에 배치
		this.setVisible(true);
		
		thread = new Thread() {
			public void run() {                                                 // 게임 진행을 위한 스레드(주기적인 down key 발생)
				try {                    
					while (true) {
						Thread.sleep(500);
						GameScreen.this.sendKey(KeyEvent.VK_DOWN);
					}
				} catch (InterruptedException e) {}
				
				SwingUtilities.invokeLater(new Runnable() {                     // 게임 종료를 알리기위함 스레드 
				    public void run() {
				    	JOptionPane.showMessageDialog(null, "GAME OVER!");
				    }
				});
			}
		};
	}
    
    class StartButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			client.send(protocol.GAME_START_REQUEST(client.userName, roomNumber));
			GameScreen.this.requestFocusInWindow();
			JButton button = (JButton) ae.getSource();
			button.setEnabled(false);
		}
    }
    
    class ChatButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			GameScreen gs = GameScreen.this;
			String message = chatTF.getText();
			
			if( !message.equals("") ) {
				client.send(protocol.ROOM_CHAT_REQUEST(client.userName, roomNumber, message));
				gs.requestFocusInWindow();
				chatTF.setText("");
			}
		}
    }
    
    class GameOverHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			thread.interrupt();                         						// 스레드의 수행을 가로챔 : InterruptedException가 발생되어 SwingUtilities.invokeLater(new Runnable())이 수행
			GameScreen gs = GameScreen.this;
			gs.removeKeyListener(gs);                   						// 게임이 끝나면 더 이상 keyEvent가 발생되지 않도록 제거
		}
    }
    
    class IntBlocReuqsetHandler implements ActionListener {
    	public void actionPerformed(ActionEvent ae) {
			client.send(protocol.GAME_INIT_BLOCK_REQUEST(
					client.userName, roomNumber));
		}
    }
    
    class MyWindowHandler implements WindowListener {
		public void windowActivated(WindowEvent we) {}
		public void windowClosed(WindowEvent we) {}
		public void windowClosing(WindowEvent we) {
			client.send(protocol.ROOM_OUT_REQUEST(client.userName, roomNumber));// 방을 나갈시 ReadyScreen을 다시 불러오기 위함
			client.send(protocol.ROOM_LIST_REQUEST(client.userName));
			client.send(protocol.USER_LIST_REQUEST(client.userName));
			client.RS.setState(Frame.NORMAL);
		}
		public void windowDeactivated(WindowEvent we) {}
		public void windowDeiconified(WindowEvent we) {}
		public void windowIconified(WindowEvent we) {}
		public void windowOpened(WindowEvent we) {}
    }
    
    public void keyPressed(KeyEvent e) { 										// Key Tx
		sendKey(e.getKeyCode());
	}
	
	public void sendKey(int key) { 												// Down Key Tx
		client.send(protocol.GAME_KEYEVENT_REQUEST(client.userName, roomNumber, key));
	}

	public void keyReleased(KeyEvent e) {}

	public void keyTyped(KeyEvent e) {}
	
	public void initBoardGraphics() {
		for ( int i = 0; i < 4; i++ )
			boardHolder[i] = new BoardGraphics(10);
		boardHolder[4] = new BoardGraphics(20);
		boardHolder[4].IntBlocReuqsetHandler = new IntBlocReuqsetHandler();
		boardHolder[4].board.gameOverHandler = new GameOverHandler();
		boardForUser.put(client.userName, boardHolder[4]);
	}
	
	public void myAppend(String playerName, String message) {
		chatTA.append("[ "+ playerName + " ] : " + message + "\n");
	}
	
	public void initComponents() {
    	JPanel jPanel0 = boardHolder[0];
    	JPanel jPanel1 = boardHolder[1];
    	JPanel jPanel2 = boardHolder[2];
    	JPanel jPanel3 = boardHolder[3];
    	JPanel jPanel4 = boardHolder[4];
    	startButton = new JButton();
    	startButton.setText("게임 시작");
    	
    	jScrollPane = new JScrollPane();
        chatButton = new JButton();
        chatTA = new JTextArea();
        chatTA.setEditable(false); 											// CenterTA 편집 불가.
        chatTF = new JTextField();
        chatTF.addActionListener(new ChatButtonHandler());
        chatButton.setText("전 송");
        chatButton.addActionListener(new ChatButtonHandler());     
        jLabel1 = new JLabel();
        jLabel2 = new JLabel();
        jLabel3 = new JLabel();
        jLabel4 = new JLabel();
        jLabel1.setText("Player1");
        jLabel2.setText("Player2");
        jLabel3.setText("Player3");
        jLabel4.setText("Player4");
        jScrollPane.setHorizontalScrollBar(null);
        jScrollPane.setViewportView(chatTA);
        chatTA.setColumns(20);
        chatTA.setRows(5);
    	
    	jPanel2.setBackground(java.awt.SystemColor.controlShadow);
        jPanel2.setPreferredSize(new java.awt.Dimension(100, 200));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel3.setBackground(java.awt.SystemColor.controlShadow);
        jPanel3.setPreferredSize(new java.awt.Dimension(100, 200));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 178, Short.MAX_VALUE)
        );

        jPanel4.setBackground(java.awt.SystemColor.controlShadow);
        jPanel4.setPreferredSize(new java.awt.Dimension(200, 400));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 200, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );

        jPanel0.setBackground(java.awt.SystemColor.controlShadow);
        jPanel0.setPreferredSize(new java.awt.Dimension(100, 200));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel0);
        jPanel0.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 181, Short.MAX_VALUE)
        );
        

        jPanel1.setBackground(java.awt.SystemColor.controlShadow);
        jPanel1.setPreferredSize(new java.awt.Dimension(100, 200));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addGap(73, 73, 73)
                                    .addComponent(jLabel4))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(chatTF)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chatButton, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(startButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(chatTF, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chatButton, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(startButton, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jPanel0, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
                                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
                                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE))))))
                .addContainerGap(8, Short.MAX_VALUE))
        );
        pack();
    }
}