package TetrisClient;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

class ReadyScreen extends JFrame {
	private JButtonTableCellRenderer buttonRenderer = new JButtonTableCellRenderer();
	private JButtonTableCellEditor buttonEditor = new JButtonTableCellEditor();
	private JTable table;
	JButton roomCreateButton, resetButton, rj;
	TextArea userListTA;
	String RoomTitle;
	Protocol protocol;
	Client client;
	JScrollPane scrollPane; //
	JPanel CP = new JPanel();
	String[][] data;
	DefaultTableModel model;

	public ReadyScreen(Client client, Protocol protocol) {
		super("♣ ReadyScreen ♣      " + "UserName : " + client.userName);
		this.protocol = protocol;
		this.client = client;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(700, 500);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		setUserList();
		setRoomList();
	}
	
	class RoomCreateButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			do {
				RoomTitle = JOptionPane.showInputDialog(null, "방제목을 입력하십시오.");
				
				if ( RoomTitle == null ) {
					break;
				} else if ( RoomTitle.length() == 0 ) {
					JOptionPane.showMessageDialog(null, "방 제목을 입력 하지 않았습니다.", "", JOptionPane.ERROR_MESSAGE);
				} else {
					client.send(protocol.ROOM_CREATE_REQUEST(client.userName, RoomTitle));
					break;
				}
			} while(true);
		}
	}
	
	class resetButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			removeAllRow();
			client.send(protocol.ROOM_LIST_REQUEST(client.userName));
			client.send(protocol.USER_LIST_REQUEST(client.userName));
		}
	}
	
	class RoomStateButtinHandler implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			client.send(protocol.ROOM_STATE_REQUEST(
					client.userName, table.getEditingRow() + 1));
		}
	}

	public void setUserList() { 												// userList, reset, createRoom GUI
		JPanel rightJP = new JPanel(new BorderLayout());
		
		JLabel userListLB = new JLabel("UsersList");
		rightJP.add(userListLB, "North");
		
		userListTA = new TextArea("", 1, 1, TextArea.SCROLLBARS_VERTICAL_ONLY);
		userListTA.setEditable(false);
		rightJP.add(userListTA, "Center");
		
		JPanel buttonsJP = new JPanel();
		JButton roomCreateButton = new JButton("CreateRoom");
		roomCreateButton.addActionListener(new RoomCreateButtonHandler());
		buttonsJP.add(roomCreateButton);
		JButton resetButton = new JButton("Reset");
		resetButton.addActionListener(new resetButtonHandler());	
		buttonsJP.add(resetButton);
		rightJP.add(buttonsJP, "South");
			
		add(rightJP, "East");
	}

	public void setRoomList() {
		String[] columnNames = { "No.", "Room Title", "Peoples", "" };
		this.model = new DefaultTableModel(data, columnNames);
		this.table = new JTable(this.model);
		this.table.setRowHeight(30);
		this.scrollPane = new JScrollPane(this.table);

		table.getColumnModel().getColumn(3).setCellRenderer(buttonRenderer);
		table.getColumnModel().getColumn(3).setCellEditor(buttonEditor);

		buttonEditor.addActionListener(new RoomStateButtinHandler());
		CP.add(scrollPane); //
		add(CP, "Center"); //
		setVisible(true);
	}

	public void addRow(String RoomNumber, String RoomTitle, String Peoples) {
		DefaultTableModel model = (DefaultTableModel) this.table.getModel();
		String arr[] = new String[4];
		arr[0] = RoomNumber;
		arr[1] = RoomTitle;
		arr[2] = Peoples;
		arr[3] = "참가";
		
		model.addRow(arr);
	}

	public void removeAllRow() {
		int total = model.getRowCount();
		
		if (this.model.getRowCount() == 0)
			return;
		for (int i = 0; i < total ; i++)
			this.model.removeRow(0);
	}
}
